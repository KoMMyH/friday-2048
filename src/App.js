import React from 'react';
import './App.css';

const FIELD_SIZE = 4;
const FIELD_SIZE_ARR = Array.from({length: FIELD_SIZE});

const createCellsArray = () => {
  const array = Array.from({length: FIELD_SIZE});
  for (let i = 0; i < FIELD_SIZE; ++i) {
    array[i] = Array.from({length: FIELD_SIZE});
    for (let j = 0; j < FIELD_SIZE; ++j) {
      array[i][j] = 0;
    }
  }
  return array;
}

class App extends React.Component {
  constructor(props) {
    super(props);
    document.addEventListener("keydown", this.keydownListener);
  }

  state = {
    cells: createCellsArray(),
    isGameStarted: false,
    pointsCount: 0,
    isWinner: false
  }

  getEmptyCellsCoords = () => {
    const {cells} = this.state;
    const emptyCells = [];
    for (let i = 0; i < FIELD_SIZE; ++i) {
      for (let j = 0; j < FIELD_SIZE; ++j) {
        if (!cells[i][j]) {
          emptyCells.push({
            x: j,
            y: i
          });
        }
      }
    }
    return emptyCells;
  }

  fillEmptyCell = () => {
    const { cells } = this.state;
    const emptyCellsCoords = this.getEmptyCellsCoords();
    const maxIndex = emptyCellsCoords.length;
    if (maxIndex > 0) {
      const cellToFill = Math.floor(Math.random() * maxIndex);
      const cell = emptyCellsCoords[cellToFill];
      this.setState({
        cells: [
          ...cells.slice(0, cell.y),
          [...cells[cell.y].slice(0, cell.x), 2, ...cells[cell.y].slice(cell.x + 1)],
          ...cells.slice(cell.y + 1)
        ]
      });
    }
  }

  keydownListener = (event) => {
    if (this.state.isGameStarted) {
      switch (event.code) {
        case "KeyA":
          this.moveLeft();
          break;
        case "KeyD":
          this.moveRight();
          break;
        case "KeyW":
          this.moveTop();
          break;
        case "KeyS":
          this.moveBottom();
          break;
      }
    }
  }

  moveLeft = () => {
    const { cells } = this.state;
    const newCells = JSON.parse(JSON.stringify(cells));
    for (let i = 0; i < FIELD_SIZE; ++i) {
      for (let j = 0; j < FIELD_SIZE; ++j) {
        for (let k = j - 1; k >= 0; --k) {
          if (newCells[i][k] === 0) {
            newCells[i][k] = newCells[i][j];
            newCells[i][j] = 0;
            continue;
          }
          if (newCells[i][k] === newCells[i][j]) {
            newCells[i][k] = 2 * newCells[i][j];
            newCells[i][j] = 0;
            continue;
          }
        }
      }
    }
    this.setState({
      cells: newCells,
      pointsCount: this.calcPointsCount(newCells),
      isWinner: this.isGameWinned(newCells)
    }, this.fillEmptyCell)
  }

  moveRight = () => {
    const { cells } = this.state;
    const newCells = JSON.parse(JSON.stringify(cells));
    for (let i = 0; i < FIELD_SIZE; ++i) {
      for (let j = 0; j < FIELD_SIZE; ++j) {
        for (let k = j + 1; k < FIELD_SIZE; ++k) {
          if (newCells[i][k] === 0) {
            newCells[i][k] = newCells[i][j];
            newCells[i][j] = 0;
            continue;
          }
          if (newCells[i][k] === newCells[i][j]) {
            newCells[i][k] = 2 * newCells[i][j];
            newCells[i][j] = 0;
            continue;
          }
        }
      }
    }
    this.setState({
      cells: newCells,
      pointsCount: this.calcPointsCount(newCells),
      isWinner: this.isGameWinned(newCells)
    }, this.fillEmptyCell)
  }

  moveTop = () => {
    const { cells } = this.state;
    const newCells = JSON.parse(JSON.stringify(cells));
    for (let i = 0; i < FIELD_SIZE; ++i) {
      for (let j = 0; j < FIELD_SIZE; ++j) {
        for (let k = i - 1; k >= 0; --k) {
          if (newCells[k][j] === 0) {
            newCells[k][j] = newCells[i][j];
            newCells[i][j] = 0;
            continue;
          }
          if (newCells[k][j] === newCells[i][j]) {
            newCells[k][j] = 2 * newCells[i][j];
            newCells[i][j] = 0;
            continue;
          }
        }
      }
    }
    this.setState({
      cells: newCells,
      pointsCount: this.calcPointsCount(newCells),
      isWinner: this.isGameWinned(newCells)
    }, this.fillEmptyCell)
  }

  moveBottom = () => {
    const { cells } = this.state;
    const newCells = JSON.parse(JSON.stringify(cells));
    for (let i = 0; i < FIELD_SIZE; ++i) {
      for (let j = 0; j < FIELD_SIZE; ++j) {
        for (let k = i + 1; k < FIELD_SIZE; ++k) {
          if (newCells[k][j] === 0) {
            newCells[k][j] = newCells[i][j];
            newCells[i][j] = 0;
            continue;
          }
          if (newCells[k][j] === newCells[i][j]) {
            newCells[k][j] = 2 * newCells[i][j];
            newCells[i][j] = 0;
            continue;
          }
        }
      }
    }
    this.setState({
      cells: newCells,
      pointsCount: this.calcPointsCount(newCells),
      isWinner: this.isGameWinned(newCells)
    }, this.fillEmptyCell)
  }

  calcPointsCount = cells => {
    let sumOfPoints = 0;
    for (let i = 0; i < FIELD_SIZE; ++i) {
      for (let j = 0; j < FIELD_SIZE; ++j) {
        sumOfPoints += cells[i][j];
      }
    }
    return sumOfPoints;
  }

  isGameWinned = cells => {
    for (let i = 0; i < FIELD_SIZE; ++i) {
      for (let j = 0; j < FIELD_SIZE; ++j) {
        if (cells[i][j] === 2048) {
          return true;
        }
      }
    }
    return false;
  }

  startGame = () => {
    if (!this.state.isGameStarted) {
      this.fillEmptyCell();
      this.setState({isGameStarted: true, pointsCount: 2});
    }
  }

  getCellColor = (num) => {
    switch (num) {
      case 2:
        return "";
      case 4:
        return "";
      case 8:
        return "";
      case 16:
        return "";
      case 32:
        return "";
      case 64:
        return "";
      case 128:
        return "";
      case 256:
        return "";
      case 512:
        return "";
      case 1024:
        return "";
      case 2048:
        return "";
    }
  }

  render() {
    const { cells, pointsCount, isWinner } = this.state;

    return (
      <div className={"wrapper"}>
        <div className={"field"}>
          {
            FIELD_SIZE_ARR.map((itemY, fieldY) => <div key={fieldY} className={"row"}>
              {
                FIELD_SIZE_ARR.map((itemX, fieldX) =>
                  <div key={fieldX} className={"cell"}>
                  {!!cells[fieldY][fieldX] && cells[fieldY][fieldX]}
                </div>)
              }
            </div>)
          }
        </div>
        <button onClick={this.startGame}>
          Start Game
        </button>
        <div className={"points"}>
          Points: {pointsCount}
        </div>
        {isWinner &&
          <div>
            You win!
          </div>
        }
      </div>
    )
  }
}

export default App;
